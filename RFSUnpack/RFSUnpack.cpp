// RFSUnpack.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <stdio.h>
#include <algorithm>

#include "RFSHeader.h"
#include "Utils.h"
#include "RFTTools.h"

using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	//skip first element: exec path
	for (auto i = 1; i < argc; ++i)
	{
		auto filePath = getFilePath(argv[i]);

		FILE *file = NULL;
		file = fopen(argv[i], "rb");

		if (!file)
		{
			printf("Error opening RFS file: %s\n", argv[i]);
			return 1;
		}

		DWORD blockNum;
		fread(&blockNum, sizeof(DWORD), 1, file);

		RFSBLOCKINFO *info = new RFSBLOCKINFO[blockNum];
		fread(info, sizeof(RFSBLOCKINFO), blockNum, file);

		for (auto j = 0; j < blockNum; ++j)
		{
			auto blockFilePath = filePath + '\\' + info[j].m_FileName;

			auto fileExt = getFileExt(blockFilePath);

			printf("ext: %s\n", fileExt.c_str());

			if (fileExt == "rft")
			{
				blockFilePath.erase(blockFilePath.end() - 3, blockFilePath.end());
				blockFilePath.append("DDS");
			}

			FILE *blockFile = fopen(blockFilePath.c_str(), "wb");

			if (!blockFile)
			{
				printf("Error writing to file: %s\n", blockFilePath.c_str());
				continue;
			}


			char *block = new char[info[j].m_Size];
			fread(block, sizeof(char) * info[j].m_Size, 1, file);

			if (fileExt == "rft")
				processRFTData(block, info[j].m_Size);

			fwrite(block, sizeof(char) * info[j].m_Size, 1, blockFile);

			delete[] block;
			fclose(blockFile);

			printf("Unpacked file: %s\n", blockFilePath.c_str());
		}

		delete[] info;
		fclose(file);
	}



	return 0;
}

