// RFSRepack.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "RFTTools.h"
#include "Utils.h"
#include "RFSHeader.h"

#include <vector>

using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	int repackFileIndex = -1;

	if (argc < 2)
		return 0;

	string rfsFilePath = getFilePath(argv[1]);

	auto rfsFileName = rfsFilePath + "\\PACKED.RFS";

	FILE *rfsFile = fopen(rfsFileName.c_str(), "wb");

	if (!rfsFile)
	{
		printf("Error opening RFS file: %s\n", rfsFileName.c_str());
		return 1;
	}

	//excext first exec file name
	DWORD blockNum = argc - 1;

	fwrite(&blockNum, sizeof(DWORD), 1, rfsFile);

	RFSBLOCKINFO *info = new RFSBLOCKINFO[blockNum];

	for (int i = 1; i < argc; ++i)
	{
		FILE *blockFile = fopen(argv[i], "rb");

		if (!blockFile)
		{
			printf("Error opening source file: %s\n", argv[i]);

			delete[] info;
			fclose(rfsFile);
			return 1;
		}

		ZeroMemory(info[i-1].m_FileName, RFSSTRLEN);

		auto blockFileName = getFileName(argv[i]);

		if (getFileExt(blockFileName) == "dds")
		{
			blockFileName.erase(blockFileName.end() - 3, blockFileName.end());
			blockFileName.append("RFT");
		}

		strcpy_s(info[i - 1].m_FileName, blockFileName.c_str());
			
		fseek(blockFile, 0, SEEK_END);
		info[i - 1].m_Size = ftell(blockFile);
			
		if (i > 1)
			info[i - 1].m_Offset = info[i - 2].m_Size + info[i - 2].m_Offset;
		else
			info[i - 1].m_Offset = sizeof(DWORD) + sizeof(RFSBLOCKINFO) * blockNum;

		fclose(blockFile);
	}

	fwrite(info, sizeof(RFSBLOCKINFO) * blockNum, 1, rfsFile);

	for (int i = 1; i < argc; ++i)
	{
		FILE *blockFile = fopen(argv[i], "rb");

		if (!blockFile)
		{
			printf("Error opening source file: %s\n", argv[i]);

			delete[] info;
			fclose(rfsFile);
			return 1;
		}

		char *blockData = new char[info[i - 1].m_Size];
		fread(blockData, sizeof(char) * info[i - 1].m_Size, 1, blockFile);

		if (getFileExt(argv[i]) == "dds")
			processRFTData(blockData, info[i - 1].m_Size);

		fwrite(blockData, sizeof(char) * info[i - 1].m_Size, 1, rfsFile);

		delete[] blockData;
		fclose(blockFile);

		printf("File packed: %s\n", argv[i]);
	}

	delete[] info;
	fclose(rfsFile);


	return 0;
}

