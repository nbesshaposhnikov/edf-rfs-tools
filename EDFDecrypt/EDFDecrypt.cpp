// EDFDecrypt.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "edftools.h"
#include "Utils.h"

using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	for (auto i = 1; i < argc; ++i)
	{
		auto filePath = getFilePath(argv[i]);
		auto fileExt = getFileExt(argv[i]);

		FILE *file = fopen(argv[i], "rb");

		if (!file)
		{
			printf("Error opening file: %s\n", argv[i]);
			continue;
		}

		string outFilePath = argv[i];
		outFilePath.erase(outFilePath.end() - 3, outFilePath.end());

		if (fileExt == "dat")
			outFilePath.append("edf");
		else if (fileExt == "edf")
			outFilePath.append("dat");

		FILE *outFile = fopen(outFilePath.c_str(), "wb");

		if (!outFile)
		{
			printf("Error writing to file: %s\n", outFilePath.c_str());

			fclose(file);
			continue;
		}

		fseek(file, 0, SEEK_END);
		auto size = ftell(file);

		fseek(file, 0, SEEK_SET);

		char *data = new char[size];

		fread(data, size * sizeof(char), 1, file);

		if (fileExt == "dat")
			size = edfDataCrypt(&data, size);
		else if (fileExt == "edf")
			size = edfDataDecrypt(data, size);
		
		fwrite(data, size, 1, outFile);

		delete[] data;
		fclose(outFile);
		fclose(file);
	}

	return 0;
}

