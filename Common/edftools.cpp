#include "edftools.h"

#include <stdlib.h>
#include <string.h>

const char  cryptBytes [] = {0x10,0x57,0x4F,0x74,0x67,0x6D,0x43,0x67,0x4E,0x6C,0x45,0x74,0x55,0x66,0x54,0x71,
                           0x52,0x39,0x41,0x4C,0x4C,0x5A,0x42,0x72,0x6E,0x69,0x6F,0x4F,0x69,0x7A,0x66,
                           0x63,0x76,0x37,0x38,0x42,0x47,0x48,0x57,0x6E,0x2B,0x50,0x4B,0x48,0x49,0x79,
                           0x4B,0x63,0x39,0x67,0x74,0x39,0x73,0x47,0x36,0x58,0x73,0x4C,0x65,0x35,0x55,
                           0x51,0x59,0x52,0x38,0x50,0x58,0x6D,0x6F,0x47,0x4C,0x66,0x6F,0x80,0xA6,0x90,
                           0xCD,0x8E,0x20,0x71,0x54,0x4F,0x18,0x98,0x30,0x57,0x40,0x53,0x48,0x6E,0x51,
                           0x6A,0x4C,0x4B,0x4D,0x74,0x67,0x46,0x4B,0x63,0x51,0x4A,0x4B,0x6D,0x50,0x51,
                           0x32,0x51,0x55,0x55,0x46,0x57,0x34,0x6A,0x79,0x68,0x6B,0x33,0x50,0x70,0x55,
                           0x58,0x31,0x72,0x4B,0x67,0x6B,0x45,0x74,0x4F,0x61,0x6B,0x42,0x49,0x34,0x36,
                           0x2E,0x5C,0x6D,0x61,0x70,0x5C,0x4E,0x65,0x75,0x74,0x72,0x61,0x6C,0x42,0x5C,
                           0x4E,0x65,0x75,0x74,0x72,0x61,0x6C,0x42,0x2E,0x42,0x53,0x50,0x50,0x44,0x41,
                           0x50,0x6A,0x32,0x47,0x75,0x54,0x70,0x52,0x69,0x53,0x75,0x41,0x66,0x56,0x49,
                           0x5A,0x67,0x63,0x4E,0x65,0x35,0x6C,0x5A,0x67,0x46,0x6D,0x54,0x4E,0x47,0x72,
                           0x30,0x32,0x79,0x53,0x34,0x61,0x75,0x61,0x51,0x72,0x77,0x4B,0x34,0x67,0x48,
                           0x6B,0x49,0x59,0x6F,0x54,0x61,0x79,0x34,0x68,0x4C,0x63,0x33,0x4D,0x66,0x4E,
                           0x6D,0x57,0x7A,0x44,0x65,0x4B,0x4A,0x74,0x37,0x51,0x35,0x52,0x39,0x79,0x63,
                           0x75,0x74,0x66,0x6D,0x55,0x75,0x53,0x2B,0x62,0x59,0x2B,0x39,0x39,0x7A,0x41,0xBC,0x24};

const char key [] = {0x1,0x2,0x4,0x8,0x10,0x20,0x40,0x80};

const char copyright [] = "RF Online by OdinTeam s(^O^)z";

int edfDataCrypt(char **inData, int size)
{    
    int i=0;

	//realoc fro new data
	*inData = (char *)realloc(*inData, size + 0x100 + strlen(copyright) + 4);

	char * data = *inData;

	char *cryptData = new char[0x100];

	memcpy(cryptData, cryptBytes, 0x100);

    do
    {
        char c = cryptData[(i+1)%256];
        if ( i & 1 )
			data[i] = data[i] - c;
        else
			data[i] = data[i] + c;

        ++i;
    }
    while ( i < size );

    i=0;

    do
    {
        char temp = cryptData[i];
        cryptData[i] = cryptData[i+1];
        cryptData[i+1] = temp;
        i += 2;
    }
    while ( i < 0x100 );

    i = 0;
    int j = 255;

    do
    {
        if ( i & 1 )
            cryptData[j] = cryptData[j]- key[(i+1) & 7];
        else
            cryptData[j] = cryptData[j]+ key[(i+1) & 7];

        data[size + i] = cryptData[j];

        if ( (i-1) & 1 )
            cryptData[j-1] =cryptData[j-1]- key[(i+2) & 7];
        else
            cryptData[j-1] =cryptData[j-1]+ key[(i+2) & 7];

		data[size + i + 1] = cryptData[j - 1];

        if ( (i&1) == 1 )
            cryptData[j-2] = cryptData[j-2] - key[(i+3) & 7];
        else
            cryptData[j-2] =cryptData[j-2]+ key[(i+3) & 7];

		data[size + i + 2] = cryptData[j - 2];

        if ( ((i-1)&1) == 1 )
            cryptData[j-3] =cryptData[j-3]- key[(i-4) & 7];
        else
            cryptData[j-3] =cryptData[j-3]+ key[(i-4) & 7];

		data[size + i + 3] = cryptData[j - 3];

        j-=4;
        i+=4;
    }
    while ( i < 0x100 );

	memcpy(data + strlen(copyright) + 4, data, size + 0x100);
	memcpy(data + strlen(copyright), &size, 4);
	memcpy(data, copyright, strlen(copyright));

	delete[] cryptData;

	return size + 0x100 + strlen(copyright) + 4;
}

int edfDataDecrypt(char *data, int size)
{
	int fileSize = 0;

	memcpy(&fileSize, data + strlen(copyright), 4);

	memcpy(data, data + strlen(copyright) + 4, size - strlen(copyright) - 4);

	//realloc(data, size - strlen(copyright) - 4);

	char *decryptData = new char[0x100];

    int  i = 0;
    int j = 255;
    //decripting decryptData
     do
     {
       if ( i & 1 )
		   data[fileSize + i] = data[fileSize + i] + key[(i + 1) & 7];
       else
		   data[fileSize + i] = data[fileSize + i] - key[(i + 1) & 7];

	   decryptData[j] = data[fileSize + i];

       if ( (i-1) & 1 )
		   data[fileSize + i + 1] = data[fileSize + i + 1] + key[(i + 2) & 7];
       else
		   data[fileSize + i + 1] = data[fileSize + i + 1] - key[(i + 2) & 7];

	   decryptData[j - 1] = data[fileSize + i + 1];

       if ( (i&1) == 1 )
		   data[fileSize + i + 2] = data[fileSize + i + 2] + key[(i + 3) & 7];
       else
		   data[fileSize + i + 2] = data[fileSize + i + 2] - key[(i + 3) & 7];

	   decryptData[j - 2] = data[fileSize + i + 2];

       if ( ((i-1)&1) == 1 )
		   data[fileSize + i + 3] = data[fileSize + i + 3] + key[(i - 4) & 7];
       else
		   data[fileSize + i + 3] = data[fileSize + i + 3] - key[(i - 4) & 7];

	   decryptData[j - 3] = data[fileSize + i + 3];

       j-=4;
       i+=4;
     }
     while ( i < 0x100);


    i=0;

    do
    {
		char temp = decryptData[i];
		decryptData[i] = decryptData[i+1];
		decryptData[i+1] = temp;
		i += 2;
    }
    while ( i < 0x100 );


    i=0;
    //decrypt file
     do
     {
       char c = decryptData[(i+1)%256];
       if ( i & 1 )
		   data[i] = data[i] + c;
       else
		   data[i] = data[i] - c;
       ++i;
     }
     while ( i < fileSize );

	 delete[] decryptData;
	 return fileSize;
}
