#pragma once

#include <string>
#include <xstring>

std::string getFilePath(const std::string& file);
std::string getFileExt(const std::string& file);
std::string getFileName(const std::string& file);