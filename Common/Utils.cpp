#include "Utils.h"

#include <algorithm>

using namespace std;

std::string getFilePath(const std::string& file)
{
	size_t i = file.length() - 1;

	while (file[i] != '\\' && i > 0)
		--i;
	
	return string(file.begin(), file.begin() + i);
}

std::string getFileExt(const std::string& file)
{
	size_t i = file.length() - 1;
	string ret;

	while (file[i] != '.' && i > 0)
	{
		ret.push_back(file[i]);
		--i;
	}

	reverse(ret.begin(), ret.end());
	transform(ret.begin(), ret.end(), ret.begin(), tolower);

	return ret;
}

std::string getFileName(const std::string& file)
{
	size_t i = file.length() - 1;
	string ret;

	while (file[i] != '\\' && i > 0)
	{
		ret.push_back(file[i]);
		--i;
	}

	reverse(ret.begin(), ret.end());

	return ret;
}