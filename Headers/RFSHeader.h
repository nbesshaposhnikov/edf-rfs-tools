#pragma once

#include <Windows.h>

const unsigned int RFSSTRLEN = 56;

struct RFSBLOCKINFO
{
	char	m_FileName[RFSSTRLEN];
	DWORD	m_Offset;
	DWORD	m_Size;
};